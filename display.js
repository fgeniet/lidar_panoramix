import * as THREE from './js/three.module.js';
import { CSS2DRenderer, CSS2DObject } from './js/CSS2DRenderer.js';

var container, camera, cameraOrtho, scene, sceneOrtho, renderer, labelRenderer, h, raycaster;

var sphere1, sphere2;

var minimapInfo, divMinimap, canvasMinimap;

var mouseX = 0, mouseY = 0;

var mouse = new THREE.Vector2(), INTERSECTED;

var teta = 0;
var phi = 0;

var sensiX = 0.001;
var sensiY = 0.001;

var euler = new THREE.Euler( 0, 0, 0, 'YXZ' );

var idPano = 0;

let panoCubes = [];
let detailCubes = [];

let transition = {running: false, prog: 1};

let transitionActivated = true;

let newPosition = {x: 0, y: 0, z: 0};

init();
animate();


/**
 * Create a canvas for the minimap and div to display the description of each scene. It also creates the event related to the map.
 * The minimap display the map with points symbolysing each scene.
 */
function createMapCanvas(){
    let img = new Image();
    img.onload = function(){
        // Draw minimap
        let imgWidth = this.width;
        let imgHeight = this.height;
        console.log("Minimap: " + minimapInfo.mapFile + " (" + imgWidth + "x" + imgHeight +")");
        canvasMinimap = document.createElement('canvas');
        canvasMinimap.class = "canvasMinimap"
        canvasMinimap.width = imgWidth;
        canvasMinimap.height = imgHeight;
        canvasMinimap.style.position = "fixed";
        canvasMinimap.style.bottom = "0";
        canvasMinimap.style.right = "0";
        canvasMinimap.style.background = "url(" + minimapInfo.mapFile + ") no-repeat center center";
        drawMinimap();
        document.body.appendChild(canvasMinimap);

        // Draw minimap title
        divMinimap = document.createElement('div');
        divMinimap.className = "divMinimap";
        
        divMinimap.style.position = "fixed";
        divMinimap.style.bottom = "250px";
        divMinimap.style.right = "0";
        divMinimap.style.width = (imgWidth-6)+"px";
        divMinimap.style.visibility = 'hidden';
        document.body.appendChild(divMinimap);

        // Add events
        canvasMinimap.addEventListener('click', onMapClick);
        canvasMinimap.addEventListener('mousemove', onMapMousemove);
    }
    img.src = minimapInfo.mapFile; 
}

/**
 * Draw each scene as a point on the minimap.
 * @param {Number} selectedPanoRang - ID of the selected scene (point with the cursor on it) (-1 by default)
 */
function drawMinimap(selectedPanoRang = -1) {
    let ctx = canvasMinimap.getContext('2d');
    ctx.clearRect(0, 0, canvasMinimap.width, canvasMinimap.height);
    minimapInfo.panosList.forEach(pano => {
        ctx.beginPath();
        ctx.arc(pano.position.x, pano.position.y, 5, 0, 2 * Math.PI, false);
        ctx.lineWidth = 1;
        if (pano.rang == selectedPanoRang){
            ctx.fillStyle = '#FF00FF';
        }
        else {
            ctx.fillStyle = '#FF0000';
        }
        ctx.fill();
        ctx.closePath();
        if (pano.rang == idPano){
            ctx.beginPath();
            ctx.arc(pano.position.x, pano.position.y, 8, 0, 2 * Math.PI, false);
            ctx.lineWidth = 2;
            ctx.strokeStyle = "#FF0000"
            ctx.closePath();
        }
        ctx.stroke();
    });
} 

/**
 * Called when the user click on the minimap. 
 * If the click is on a point, the user is moved to the related scene and the minimap is updated.
 * @param {event} event - Click on the map
 */
function onMapClick(event) {
    let mousePos = {x: event.clientX - this.offsetLeft, y: event.clientY - this.offsetTop};
    minimapInfo.panosList.forEach(pano => {
        if (distance(mousePos, pano.position) < 5){
            console.log(pano.desc);
            displayScene(pano.rang, transitionActivated);
        }
    });
}

/**
 * Called when the user move the cursor on the minimap. 
 * If the cursor is on a point, it changes its color and display its description in the div.
 * Else, the description div is hidden and the minimap updated. 
 * @param {event} event - Cursor movement on the map
 */
function onMapMousemove(event) {
    let mousePos = {x: event.clientX - this.offsetLeft, y: event.clientY - this.offsetTop};
    let mouseOverPano = false;
    minimapInfo.panosList.forEach(pano => {
        if (distance(mousePos, pano.position) < 5){
            drawMinimap(pano.rang);
            divMinimap.textContent = pano.desc;
            divMinimap.style.visibility = "visible";
            mouseOverPano = true;
        }
    });
    if (!mouseOverPano){
        drawMinimap();
        divMinimap.style.visibility = "hidden";
    }
}

/**
 * Return the distance between two 3D positions.
 * @param {{x: Number, y:Number, z: Number}} pos1 - First position
 * @param {{x: Number, y:Number, z: Number}} pos2 - Second position
 */
function distance(pos1, pos2) {
    return Math.sqrt(Math.pow(pos1.x - pos2.x, 2) + Math.pow(pos1.y - pos2.y, 2) )
}

/**
 * Initialyse the minimap by extrating the info of the minimap in the XML file.
 */
function dislayMap(){
    let map = xmlDoc.then(function(response){
        let mapFile = "Chapelle/" + response.getElementsByTagName("mapname")[0].childNodes[0].nodeValue.replace(" ", "");
        let panosList = []
        let panos = response.getElementsByTagName("pano");
        for (i = 0; i < panos.length; i++){
            let desc = panos[i].getElementsByTagName("desc")[0].childNodes[0].nodeValue;
            let cartex = parseInt(panos[i].getElementsByTagName("cartex")[0].childNodes[0].nodeValue);
            let cartey = parseInt(panos[i].getElementsByTagName("cartey")[0].childNodes[0].nodeValue);
            panosList.push({rang: i, desc: desc, position: {x: cartex, y: cartey}});
        }
        console.log({mapFile: mapFile, panosList: panosList});
        return {mapFile: mapFile, panosList: panosList};
        
    });
    map.then(function(result){
        minimapInfo = result;
        createMapCanvas();
    });
}

/**
 * Create a 2D label linked to a ThreeJs object.
 * It will follow the movement of this object to always be next to it.
 * @param {String} message - Message that the label will display
 * @param {THREE.Mesh} object - Object related to the label
 * @param {String} classDiv - Class of the div (to use the correct css)
 */
function addLabel( message, object, classDiv)
{
    var labelDiv = document.createElement('div');
    labelDiv.className = classDiv;
    labelDiv.textContent = message;
    var label = new CSS2DObject(labelDiv);
    label.position.set( 0, 0, 0 );
    object.add(label);
}


/**
 * Move the user to a new scene. Transition can be displayed.
 * @param {Number} id - ID of the scene
 * @param {Boolean} transitionActivated - If the transition will be activated or not.
 */
function displayScene(id, transitionActivated=false){
    resetScene();
    if(transitionActivated){
        displayPanoWithTransition(id)
    }
    else{
        displayPano(id);
    }
    displayOtherPano(id);
    displayDetails(id);
}

/**
 * Reset the scene by deleting cubes representing the other scenes and the details
 */
function resetScene(){
    for (let i=0; i < panoCubes.length; i++){
        panoCubes[i].geometry.dispose();
        panoCubes[i].material.dispose();
        scene.remove(panoCubes[i]);
    }
    for (let i=0; i < detailCubes.length; i++){
        detailCubes[i].geometry.dispose();
        detailCubes[i].material.dispose();
        scene.remove(detailCubes[i]);
    }
    panoCubes.length = 0;
    detailCubes.length = 0;
    console.log("resetScene");
}

/**
 * Display a pano without a transition (image projected on a sphere)
 * @param {Number} id - ID of the pano
 */
function displayPano(id){
    getPanoInfo(id)
    .then(function(panoInfo){

        sphere1.geometry.dispose();
        sphere1.material.dispose();
        scene.remove(sphere1);

        console.log("Chapelle/"+panoInfo.img);
        let texture = new THREE.TextureLoader().load("Chapelle/"+panoInfo.img);
        texture.wrapS = THREE.RepeatWrapping;
        texture.repeat.x = - 1;
        let geometry = new THREE.SphereGeometry( 2, 32, 32 );
        let material = new THREE.MeshPhongMaterial( {map: texture, color: 0xffffff, side : THREE.BackSide} );
        sphere1 = new THREE.Mesh( geometry, material );
        sphere1.rotateY(-panoInfo.v0);
        scene.add(sphere1);
        idPano = id;
        if (canvasMinimap){
            drawMinimap();
        }

    });
}

/**
 * Display a pano with a transition if the two scenes are intervisible
 * @param {Number} id - ID of the pano
 */
function displayPanoWithTransition(id){
    getPanoInfo(id)
    .then(function(panoInfo){
        console.log("Chapelle/"+panoInfo.img);
        let texture = new THREE.TextureLoader().load("Chapelle/"+panoInfo.img);
        texture.wrapS = THREE.RepeatWrapping;
        texture.repeat.x = - 1;
        let geometry = new THREE.SphereGeometry( 2, 32, 32 );
        let material = new THREE.MeshPhongMaterial( {map: texture, color: 0xffffff, side : THREE.BackSide} );  
        sphere2 = new THREE.Mesh( geometry, material );
        sphere2.rotateY(-panoInfo.v0);
        getPanoPosition(idPano, id)
        .then(function(positionPano){
            newPosition = positionPano;
            sphere2.position.x = positionPano.x;
            sphere2.position.y = positionPano.y;
            sphere2.position.z = positionPano.z;
            scene.add(sphere2);
            if (positionPano.x == 0 && positionPano.y == 0  && positionPano.z == 0 ){
                transition.prog = 0;
            }
            transition.running = true;
            idPano = id;
            if (canvasMinimap){
                drawMinimap();
            }

        });
    });

}

/**
 * Display a frame of the transition animation
 * @param {Number} speed - Speed of the transition
 */
function execTransition(speed){
    if (transition.prog > 0){
        sphere2.position.x = transition.prog*newPosition.x;
        sphere2.position.y = transition.prog*newPosition.y;
        sphere2.position.z = transition.prog*newPosition.z;
        sphere1.position.x = -(1-transition.prog)*newPosition.x;
        sphere1.position.y = -(1-transition.prog)*newPosition.y;
        sphere1.position.z = -(1-transition.prog)*newPosition.z;
        transition.prog -= speed;
    }
    else{
        transition = {running: false, prog: 1};
        [sphere1, sphere2] = [sphere2, sphere1];
        sphere2.geometry.dispose();
        sphere2.material.dispose();
        scene.remove(sphere2);
        sphere1.position.x = 0;
        sphere1.position.y = 0;
        sphere1.position.z = 0;

    }
}

/**
 * Display the intervisible panos as a cube
 * @param {Number} id - ID of the pano
 */
function displayOtherPano(id){
    getOtherPanos(id)
    .then(function(otherPanos){
        for (let i = 0; i < otherPanos.length; i++){
            displayCubePano(otherPanos[i]);
        }
    });
}

/**
 * Display the details visible from the scene as a cube 
 * @param {Number} id - ID of the pano
 */
function displayDetails(id){
    getDetails(id)
    .then(function(details){
        for (let i = 0; i < details.length; i++){
            displayCubeDetail(details[i]);
        }
    });
}

/**
 * Create a cube representing a pano
 * @param {{id: Number, x: Number, y: Number, z: Number}} pano - Info of the represented pano
 */
function displayCubePano(pano){

    let geometry = new THREE.BoxGeometry(0.04, 0.04, 0.04, 1, 1, 1);
    let material = new THREE.MeshPhongMaterial(
        {color: 0xff0000,
        opacity: 0.5,
        transparent: true
    });    
    let cube = new THREE.Mesh( geometry, material );
    cube.position.z = pano.x;
    cube.position.x = pano.y;
    cube.position.y = pano.z;
    cube.typeObject = "pano";
    cube.rang = pano.id;
    panoCubes.push(cube);
    scene.add(cube);
}

/**
 * Create a cube representing a detail
 * @param {{titre: String, url: String, x: Number, y: Number, z: Number}} detail - Info of the represented detail
 */
function displayCubeDetail(detail){

    let geometry = new THREE.BoxGeometry(0.02, 0.02, 0.02, 1, 1, 1);
    let material = new THREE.MeshPhongMaterial(
        {color: 0x00ff00,
        opacity: 0.5,
        transparent: true
    });    
    let cube = new THREE.Mesh( geometry, material );
    cube.position.z = detail.x;
    cube.position.x = detail.y;
    cube.position.y = detail.z;
    cube.typeObject = "detail";
    cube.titre = detail.titre;
    cube.url = detail.url;
    detailCubes.push(cube);
    scene.add(cube);
}

/**
 * Initialyse the application by creating the basic ThreeJs objects and the minimap. It will also displayed the first pano listed in the XML description file.
 */
function init() {

    // Creation of the container
    container = document.createElement( 'div' );
    document.body.appendChild( container );

    // Creation of the camera
    camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 30000 );
    scene = new THREE.Scene();

    // Create pano
    let geometry = new THREE.SphereGeometry( 2, 32, 32 );
    let material = new THREE.MeshPhongMaterial( {color: 0xffffff, side : THREE.BackSide} );    
    sphere1 = new THREE.Mesh( geometry, material );
    scene.add( sphere1 );

    var ambientLight = new THREE.AmbientLight( 0xffffff, 0.3);
    scene.add(  ambientLight );
    var pointLight = new THREE.PointLight( 0xffffff, 0.6, 100 );
    scene.add( pointLight );

    displayScene(0);

    // Display map
    cameraOrtho = new THREE.OrthographicCamera( -window.innerWidth / 2, window.innerWidth / 2, window.innerHeight / 2, - window.innerHeight / 2, 1, 10 );
    cameraOrtho.position.z = 10;
    sceneOrtho = new THREE.Scene();
    dislayMap();

    raycaster = new THREE.Raycaster();

    renderer = new THREE.WebGLRenderer( { antialias: true } );
    renderer.autoClear = false;
	renderer.setSize( window.innerWidth, window.innerHeight );
    document.body.appendChild( renderer.domElement );
    renderer.render( scene, camera );

    labelRenderer = new CSS2DRenderer();
    labelRenderer.setSize( window.innerWidth, window.innerHeight );
    labelRenderer.domElement.style.position = 'absolute';
    labelRenderer.domElement.style.top = 0;
    document.body.appendChild( labelRenderer.domElement );

    document.addEventListener( 'mousedown', onDocumentMouseDown, false );
    document.addEventListener('wheel', onDocumentMouseWheel);
    document.addEventListener('mousemove', updateMousePosition, false);
    document.addEventListener('click', onDocumentClick, false);
      
    window.addEventListener( 'resize', onWindowResize, false );
}

/**
 * Called when the user click on the ThreeJs scene. If the user clicks on a pano cube, he is moved to it.
 * @param {Event} event - Click on the ThreeJs scene
 */
function onDocumentClick(event) {
    if (INTERSECTED != null) {
        if (INTERSECTED.typeObject == "pano"){
            console.log(INTERSECTED.rang);
            displayScene(INTERSECTED.rang, transitionActivated);
        }
        else if (INTERSECTED.typeObject == "detail"){
            console.log(INTERSECTED.titre);
            console.log(INTERSECTED.url);
            window.open("Chapelle/" + INTERSECTED.url); 
        }

    }
}

/**
 * Called when the user moves the cursor on the ThreeJs scene. Update the mouse position.
 * @param {Event} event -  Mouse movement on the ThreeJs scene
 */
function updateMousePosition(event) {
    mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

/**
 * Called when the user pushes a mouse button on the ThreeJs scene. Create the event needed to correctly move the camera.
 * @param {Event} event - Push on a mouse button on the ThreeJs scene
 */
function onDocumentMouseDown( event ) {
    mouseX = event.clientX;
    mouseY = event.clientY;
    document.addEventListener('mousemove',onDocumentMouseMove);
    document.addEventListener('mouseup',function(event){
        document.removeEventListener("mousemove",onDocumentMouseMove, false);
    });
}

/**
 * Called when the user moves the cursor on the ThreeJs scene. Update the angles of the camera.
 * @param {Event} event - Mouse movement on the ThreeJs scene
 */
function onDocumentMouseMove( event ) {

    let dx = event.clientX-mouseX;
    let dy = event.clientY-mouseY;

    teta =teta+sensiX*dx;


    let newPhi =phi+sensiY*dy;
    if (-Math.PI/2<newPhi && newPhi<Math.PI/2){
        phi = newPhi;
    }

    mouseX = event.clientX;
    mouseY = event.clientY;
}

/**
 * Called when the user uses the wheel of the mouse on the ThreeJs scene. Update the zoom.
 * @param {Event} event - Use of the mouse wheel on the ThreeJs scene
 */
function onDocumentMouseWheel( event ){
    const delta = Math.sign(event.deltaY);
    camera.fov = Math.max(Math.min(camera.fov +delta*3, 90), 10);
    camera.updateProjectionMatrix();
}

/**
 * Called when the user resize the window. Update the camera, the ThreeJs objects and the labels related to them.
 * @param {Event} event - Resizing of the window
 */
function onWindowResize( event ) {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
    labelRenderer.setSize( window.innerWidth, window.innerHeight );

}

/**
 * Called at each frame. Compute and display the animations.
 */
function animate() {

    panoCubes.forEach(cube => {
        cube.rotation.x += 0.01; 
        cube.rotation.y += 0.01;});
    detailCubes.forEach(cube => {
        cube.rotation.x += 0.01; 
        cube.rotation.y += 0.01;});
    requestAnimationFrame( animate );

    if (transition.running){
        execTransition(0.02);
    }

    render();
}

/**
 * Render the ThreeJs scene
 */
function render() {
    euler.x = phi;
    euler.y = teta;

    camera.quaternion.setFromEuler( euler );
    camera.updateMatrix();

    
    // find intersections

    raycaster.setFromCamera( mouse, camera );

    var intersects = raycaster.intersectObjects( scene.children );

    if ( intersects.length > 1 ) {

        if ( INTERSECTED != intersects[ 0 ].object && (intersects[ 0 ].object.typeObject == "pano" || intersects[ 0 ].object.typeObject == "detail")) {

            if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );

            INTERSECTED = intersects[ 0 ].object;
            INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
            INTERSECTED.material.emissive.setHex( 0x0000ff );

            if (INTERSECTED.typeObject == "pano"){
                addLabel("Pano " + INTERSECTED.rang, INTERSECTED, "labelPano");
            }
            else if (INTERSECTED.typeObject == "detail"){
                addLabel(INTERSECTED.titre, INTERSECTED, "labelDetail");
            }

        }

    } else {

        if ( INTERSECTED ) INTERSECTED.material.emissive.setHex( INTERSECTED.currentHex );
        if (INTERSECTED != null){
            var listLabelPano = document.getElementsByClassName("labelPano");
            for (var i = 0; i < listLabelPano.length; i++){
                listLabelPano[i].remove();
            }
            var listLabelDetail = document.getElementsByClassName("labelDetail");
            for (var i = 0; i < listLabelDetail.length; i++){
                listLabelDetail[i].remove();
            }
            INTERSECTED.children.length = 0;
            INTERSECTED = null;
        }
    }

    renderer.clear();
    renderer.render( scene, camera );
    renderer.clearDepth();
    renderer.render( sceneOrtho, cameraOrtho );
    
    labelRenderer.render( scene, camera );
    
}
