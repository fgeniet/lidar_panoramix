# Projet visite panoramique

Projet TSI C 2019-2020

## Contexte

Dans le cadre des stages terrain en photogrammétrie et lasergrammétrie, l'ENSG réalise chaque année des acquisitions d'images panoramiques HR ainsi que des levés 3D à l'aide d'un scanner laser terrestre.

Ces deux types de données permettent de générer des jeux d'images sphériques équirectangulaires constituant une archive documentaire immersive du chantier.

Pour faciliter la visualisation de ces données, une application Flash a été développée, offrant plusieurs fonctionnalités :

 - visualisation d'images panoramiques
 - affichage de points d'intérêts (avec liens intéractifs)
 - possibilité de se déplacer entre les images panoramiques
 - minimap interactive
 - transition animée durant les déplacements (sur certains jeux de données)

## Mission

Notre mission au cours de ce projet est donc de développer un nouveau site web, basé Three.js en particulier. Ce dernier devra permettre de visiter de scènes reconstituées à l'aide d'acquisitions par scanner laser terrestre et d'acquisitions d'images panoramiques HR.

## Lancement

Pour lancer le site en local, la solution la plus simple est d'utiliser un http-server.

Tout d'abord, si ce n'est pas fait, installer npm :

Sous Linux : `sudo apt-get install npm`

Sous Windows : [site web de node](https://nodejs.org)

Ensuite installer http-server : `sudo npm install http-server`

Enfin, se placer à la racine du projet et lancer http-server : 
```
cd [chemin du dossier]
http-server
```

Le site est alors acessible à l'adresse suivante : http://127.0.0.1:8080

## Fonctionnement

Pour reconstituer une scène, on dispose de différentes images panoramiques, prises depuis différents points de vus connus, ainsi qu'un fichier XML de description permettant d'associer à chaque image sa position, son orientation, ainsi que les éléments accessibles depuis celle-ci.

### Affichage de scène 3D

Pour créer notre scène 3D, une image panoramique est projetée sur une sphère de rayon 2, centrée sur la caméra. Les éléments intéractifs, tels que les détails et les images panoramiques intervisibles, sont représentés par des cubes placés sur une sphère de rayon 1. Survoler ces éléments avec la souris les met en surbrillance et fait apparaître leur label.

Les cubes verts représentent les détails visibles sur l'image. En cliquant dessus, on fait apparaître leur fiche de renseignement.

Les cubes rouges représentent les scènes panoramiques accessibles depuis la scène actuelle.

### Contrôles

Pour permettre à l'utilisateur d'observer la scène panoramique, nous avons conservé les même constrôles que l'application précédente, basés sur le drag souris. Nous avons toutefois ajouté des bornes sur la rotation verticale pour empêcher l'utilisateur de retourner la caméra.

Il est aussi possible pour l'utilisateur de bénificier d'un zoom avec la mollette souris en jouant sur la focale de la caméra. Comme précédemment, nous avons ajouté des bornes pour éviter les focales aberrantes qui pouvaient apparaître sur la version originale.

### Minimap interactive

Notre application présente aussi une minimap interactive. Cette dernière permet de visualiser la position de l'utilisateur, d'afficher les descriptions des scènes panoramiques et de se déplacer d'une scène à l'autre.

### Déplacement entre les scènes

Nous avons implémentés deux méthodes de déplacement différentes.

Dans la première méthode, il n'y a pas de transition particulière lors du déplacement. Il suffit donc de modifier l'image affichée sur la sphère et charger les nouveaux éléments intéractifs.

Dans la seconde méthode, nous avons mis en place une transition animée pour rendre le déplacement moins abrupte. L'animation présente dans l'application Flash n'était pas très naturelle, car elle faisait intervenir une rotation, pouvant désorienter l'utilisateur. Nous avons décidé de nous baser sur un autre modèle et avons choisi Google Street Map, dont l'animation mettant en scène en déplacement rectiligne est beaucoup plus naturelle.

Pour simuler ce déplacement, nous utilisons deux sphères : celle dans laquelle l'utilisateur est présent, et celle dans laquelle l'utilisateur veut se déplacer. On fait alors glisser les deux sphères sur l'axe formé par leur centre respectif jusqu'à ce que la deuxième remplace la première.

## Problèmes observés et pistes d'améliorations

Un problème observé est que, dans certains cas, la méthode de déplacement ne rend pas bien l'illusion du mouvement. En effet, quand on se rapproche du bord de la première boule, on zoom au maximum sur les détails affichés sur la boule (le fond de la scène), et non sur la nouvelle position de prise de vue.

De plus, on ne connait pas la distance entre ces détails de fond de scène et les différents points de prise de vue, donc impossible à priori de savoir de quelle distance il faut se déplacer dans la sphère pour arriver à bien simuler l'arrivée sur le nouveau point de vue.

Pour diminuer l'impression de se déplacer vers le fond puis de revenir en arrière, on pourrait choisir une distance arbitraire (par exemple la moitié du rayon de la sphère), mais cela ne ferait qu'adoucir un peu le problème.

Un autre problème est que lorsqu'on fait bouger la sphère pour ce changement de position, l'image se déforme (du au fait que l'on est plus au centre). Pour obtenir un meilleur rendu, on pourrait utiliser des cartes de profondeur comme textures bumpmap en plus des images panoramiques comme textures.

## Auteurs

Hugo De Paulis     
Florent Geniet